# Équations

Exerciseur développé par Rémi Angot sur les équations à tester sur [coopmaths.fr/apps/equations](https://coopmaths.fr/apps/equations)

## Utiliser en local

```
    pnpm install
    pnpm start
```
