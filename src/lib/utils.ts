export function randInt (min: number, max: number, avoid: number[] = []) {
  let result = Math.floor(Math.random() * (max - min + 1) + min)
  while (avoid.includes(result)) {
    result = Math.floor(Math.random() * (max - min + 1) + min)
  }
  return result
}