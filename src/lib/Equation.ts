import Trinome from './Trinome'

export default class Equation {
  left: Trinome
  right: Trinome
  constructor (left: Trinome, right: Trinome) {
    this.left = left
    this.right = right
  }

  toString (operation?: 'add'|'mul'|'div', a?: number, x?: boolean): string {
    return `$${this.left.toString(operation, a, x)} = ${this.right.toString(operation, a, x)}$`
  }

  add (k: number | Trinome): Equation {
    if (typeof k === 'number') k = new Trinome(0, 0, k)
    return new Equation(this.left.add(k), this.right.add(k))
  }

  sub (k: number | Trinome): Equation {
    if (typeof k === 'number') k = new Trinome(0, 0, k)
    return new Equation(this.left.sub(k), this.right.sub(k))
  }

  mul (k: number): Equation {
    return new Equation(this.left.mul(k), this.right.mul(k))
  }

  div (k: number): Equation {
    return new Equation(this.left.div(k), this.right.div(k))
  }

  isSolved (): false | Trinome {
    const x = new Trinome(0, 1, 0)
    if (this.left.isEqual(x) && this.right.degree === 0) return this.right
    if (this.right.isEqual(x) && this.left.degree === 0) return this.left
    return false
  }
}
