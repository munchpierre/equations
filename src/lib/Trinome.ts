import Fraction from 'fraction.js'

export default class Trinome {
  a: Fraction
  b: Fraction
  c: Fraction
  constructor (a: Fraction | number, b: Fraction | number, c: Fraction | number) {
    if (typeof a === 'number') a = new Fraction(a)
    if (typeof b === 'number') b = new Fraction(b)
    if (typeof c === 'number') c = new Fraction(c)
    this.a = a
    this.b = b
    this.c = c
  }

  add (trinome: Trinome): Trinome {
    return new Trinome(this.a.add(trinome.a), this.b.add(trinome.b), this.c.add(trinome.c))
  }

  sub (trinome: Trinome): Trinome {
    return new Trinome(this.a.sub(trinome.a), this.b.sub(trinome.b), this.c.sub(trinome.c))
  }

  mul (k: number | Fraction): Trinome {
    return new Trinome(this.a.mul(k), this.b.mul(k), this.c.mul(k))
  }

  div (k: number | Fraction): Trinome {
    return new Trinome(this.a.div(k), this.b.div(k), this.c.div(k))
  }

  isEqual (trinome: Trinome): boolean {
    return this.a.equals(trinome.a) && this.b.equals(trinome.b) && this.c.equals(trinome.c)
  }

  toString (operation?: 'add'|'mul'|'div', a?: number, x?: boolean): string {
    let result = ''
    if (this.a.valueOf() === 0 && this.b.valueOf() === 0 && this.c.valueOf() === 0 ) {
      result = '0'
    } 
    if (this.a.valueOf() !== 0) {
      result += `${display(this.a)}x^2`
    }
    if (this.b.valueOf() > 0 && this.a.valueOf() !== 0) {
      result += '+'
    }
    if (this.b.valueOf() !== 0) {
      result += `${display(this.b)}x`
    }
    if (this.c.valueOf() > 0 && (this.a.valueOf() !== 0 || this.b.valueOf() !== 0)) {
      result += '+'
    }
    if (this.c.valueOf() !== 0) {
      result += `${display(this.c, false)}`
    }
    if (operation === 'add' && a !== undefined && !x) {
      if (a > 0) result = `\\left( ${result} \\right) \\textcolor{#F15929}{\\boldsymbol{~+~${a}}}`
      if (a < 0) result = `\\left( ${result} \\right) \\textcolor{#F15929}{\\boldsymbol{~-~${-a}}}`
    }
    if (operation === 'add' && a !== undefined && x) {
      let aString = ''
      if (a === 1) {
        aString = '~+~x'
      } else if (a === -1) {
        aString = '~-~x'
      } else if (a > 0) {
        aString = `~+~${a}x`
      } else {
        aString = `~-~${-a}x`
      }
      result = `\\left( ${result} \\right) \\textcolor{#F15929}{\\boldsymbol{${aString}}}`
    }
    if (operation === 'mul' && a !== undefined) {
      if (a > 0) result = `\\left(${result}\\right) \\textcolor{#F15929}{\\boldsymbol{~\\times~${a}}}`
      if (a < 0) result = `\\left(${result}\\right) \\textcolor{#F15929}{\\boldsymbol{~\\times~\\left(${a}\\right)}}}`
    }
    if (operation === 'div' && a !== undefined) {
      if (a > 0) result = `\\left(${result}\\right) \\textcolor{#F15929}{\\boldsymbol{~\\div ${a}}}`
      if (a < 0) result = `\\left(${result}\\right) \\textcolor{#F15929}{\\boldsymbol{~\\div \\left(${a}\\right)}}`
    }
    return result
  }

  get degree (): number {
    if (this.a.valueOf() !== 0) return 2
    if (this.b.valueOf() !== 0) return 1
    return 0
  }

  static createAx (a: number) {
    return new Trinome(0, a, 0)
  }

  static createAxPlusB (a: number, b: number) {
    return new Trinome(0, a, b)
  }

  static createConstant (c: number) {
    return new Trinome(0, 0, c)
  }
}

function display (a: Fraction, isBeforeALetter = true): string {
  let aString = ''
  if (a.valueOf() === 0) return ''
  if (Number.isInteger(a.valueOf())) {
    aString = a.toString()
  } else {
    if (a.s === -1) {
      aString = '-'
    }
    aString += `\\dfrac{${a.n}}{${a.d}}`
  }
  if (!isBeforeALetter) {
    return aString
  } else {
    if (aString === '1') {
      return ''
    }
    if (aString === '-1') {
      return '-'
    }
    return aString
  }
}
